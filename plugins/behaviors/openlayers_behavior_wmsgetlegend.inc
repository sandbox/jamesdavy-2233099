<?php
// $Id$

/**
 * @file
 * Implementation of OpenLayers behavior.
 */

/**
 * WMSgetlegend Behavior
 */

$plugin = array(
    'openlayers_behavior_wmsgetlegend' => array(
        'title' => t('Get WMS Legend'),
        'description' => t('Queries WMS server for Legend.'),
        'type' => 'layer',
        'behavior' => array(
          'file' => 'openlayers_behavior_wmsgetlegend.inc',
          'class' => 'openlayers_behavior_wmsgetlegend',
          'parent' => 'openlayers_behavior',
        ),
    ),
);


class openlayers_behavior_wmsgetlegend extends openlayers_behavior {
  /**
   * Provide initial values for options.
   */
  function options_init() {
    return array();
  }

  function options_form($defaults) {
    $info_formats = array(
      'image/png' => 'image/png',
      'text/html' => 'text/html',
      'application/vnd.ogc.gml' => 'application/vnd.ogc.gml',
    );
    // TODO only return form when there is at least 1 WMS layer
    return array(
      'getlegend_usevisiblelayers' => array(
        '#title' => "Use All Visible Layers",
        '#type' => 'checkbox',
        '#description' => t('Query all visible WMS layers on the map.'),
        '#default_value' => isset($defaults['getlegend_usevisiblelayers']) ?
          $defaults['getlegend_usevisiblelayers'] : TRUE,
      ),
      'getlegend_layers' => array(
        '#title' => "Only Query",
        '#type' => 'select',
        '#options' => $this->map['layers'], // TODO filter this on WMS layers only
        '#description' => t("Select the layer from which points are pulled. This must be a WMS layer. 'Use Visible Layers' must be unchecked for this option to have effect"), //TODO Fix this in javascript
        '#default_value' => isset($defaults['getlegend_layers']) ?
          $defaults['getlegend_layers'] : NULL,
        '#states' => array(
          // Only show this field when the region is selected.
          'visible' => array(
            ':input[name="behaviors[openlayers_behavior_wmsgetlegend][options_set][options][getlegend_usevisiblelayers]"]' => array('checked' => FALSE),
          ),
        ),
      ),
      'getlegend_info_format' => array(
        '#title' => "Format ",
        '#type' => 'select',
        '#options' => $info_formats,
        '#description' => t("The format the server should return. text formats are displayed as-is. The Gml
          format is displayed in a Drupal table, and themeable."),
        '#default_value' => isset($defaults['getlegend_info_format']) ?
          $defaults['getlegend_info_format'] : NULL,
      ),
      'getlegend_htmlelement' => array(
        '#type' => 'textfield',
        '#description' => t("An HTML element (#id!) that will be filled
          with the query result. Ommit the # here. If you use the getlegend block, the value should be 'getlegend'"),
        '#default_value' => isset($defaults['getlegend_htmlelement']) ?
          $defaults['getlegend_htmlelement'] : "getlegend",
      ),
    );
  }

  /**
   * Render.
   */
  function render(&$map) {
    drupal_add_js(drupal_get_path('module', 'wms_legend') . '/plugins/behaviors/js/openlayers_behavior_wmsgetlegend.js');
    return $this->options;
  }
}

