// $Id$

/**
 * @file
 * JS Implementation of OpenLayers behavior.
 */

/**
 * WMSgetlegend Behavior
 * http://dev.openlayers.org/releases/OpenLayers-2.9/doc/apidocs/files/OpenLayers/Control/WMSGetFeatureInfo-js.html
 */

(function ($) {
  //Initialize settings array.
  Drupal.openlayers.openlayers_behavior_wmsgetlegend = Drupal.openlayers.openlayers_behavior_wmsgetlegend || {};

  //Add the wmsgetlegend behavior
  Drupal.openlayers.addBehavior('openlayers_behavior_wmsgetlegend', function (data, options) {
    var map = data.openlayers;
    var layer;
    var layers = [];

    if (data.map.behaviors['openlayers_behavior_wmsgetlegend']) {
      if (options.getlegend_usevisiblelayers == false) { 
        layers = data.openlayers.getLayersBy('drupalID', 
          options.getlegend_layers);  // TODO Make this multiple select! 
      } else {
        for (layer in data.openlayers.layers) {
          if ((data.openlayers.layers[layer].CLASS_NAME == "OpenLayers.Layer.WMS") &&
              (data.openlayers.layers[layer].isBaseLayer == false)){
            layers.push(data.openlayers.layers[layer]);
          }
        }
      }

      Drupal.openlayers.openlayers_behavior_wmsgetlegend.getlegend_htmlelement = 
        options.getlegend_htmlelement;
      Drupal.openlayers.openlayers_behavior_wmsgetlegend.getlegend_usevisiblelayers = 
        options.getlegend_usevisiblelayers;
      Drupal.openlayers.openlayers_behavior_wmsgetlegend.getlegend_info_format = 
        options.getlegend_info_format;
      Drupal.openlayers.openlayers_behavior_wmsgetlegend.layers = layers;

      OpenLayers.Control.Click = OpenLayers.Class(OpenLayers.Control, 
        {
          defaultHandlerOptions: {
            'single': true,
            'double': false,
            'pixelTolerance': 0,
            'stopSingle': false,
            'stopDouble': false
          },

          initialize: function(options) {
            this.handlerOptions = OpenLayers.Util.extend(
              {}, this.defaultHandlerOptions
            );
            OpenLayers.Control.prototype.initialize.apply(
              this, arguments
            ); 
            this.handler = new OpenLayers.Handler.Click(
              this, {
                'click': this.onClick,
              }, this.handlerOptions
            );
          }, 

          onClick: function(evt) {
            //map = data.openlayers;
            var layernames = "";
            var wmslayers = "";
            var projection = "";
			var layerurl = "";
			var url = "";
			//var x = 0;
            // TODO check if all layers have same projection
            // TODO check if all layers are from same server
            for (layer in Drupal.openlayers.openlayers_behavior_wmsgetlegend.layers) {
              if (layers[layer].visibility 
                  && (layers[layer].CLASS_NAME == "OpenLayers.Layer.WMS") &&
                  layers[layer].isBaseLayer == false ) {
                wmslayers = layers[layer].params.LAYERS;
                layernames = layernames + " " + layers[layer].name;
                projection = layers[layer].projection['projCode']; 
				layerurl = layers[layer].url;
				//x = x+1;
				//alert (wmslayers)
				if (wmslayers.length > 0) { 
             // wmslayers = wmslayers.substring(0, wmslayers.length -1);
              Drupal.openlayers.openlayers_behavior_wmsgetlegend.beforegetlegend(layernames);
              var params = {
                    REQUEST: "GetLegendGraphic",
                    EXCEPTIONS: "application/vnd.ogc.se_xml",
                    INFO_FORMAT: Drupal.openlayers.openlayers_behavior_wmsgetlegend.getlegend_info_format, 
                    LAYER: wmslayers
					}; // TODO parameterize this, like ...?
               var tempurl = layers[0].getFullRequestString(params);
			   var urlarray = tempurl.split("?");
			   var tempurl2 = layerurl+urlarray[1];
			   url = url+tempurl2+";"+layers[layer].name+"|";
			   //alert(url);
               $.ajax({ 
                 type: 'POST', 
                 url: Drupal.settings.basePath + 'openlayers/wms_legend/wmsgetlegend',
                 data: { 
                   ajax : true, 
                   url : url 
                 },
                 success: Drupal.openlayers.openlayers_behavior_wmsgetlegend.fillHTML,
                 fail: Drupal.openlayers.openlayers_behavior_wmsgetlegend.fillHTMLerror
               });
            }
            else {
              //alert ("No Layer to Query is visible.");
            }
              }
            }
            
          }
        }
      );
      GetFeatureControl = new OpenLayers.Control.Click(); 

      data.openlayers.addControl(GetFeatureControl);
      GetFeatureControl.activate();
    
      // This is to update the OpenLayers Plus block switcher feature
      // Drupal.OpenLayersPlusBlockswitcher.redraw();  // TODO Fix this for D7
    }
  });

  Drupal.openlayers.openlayers_behavior_wmsgetlegend.beforegetlegend = function(layernames) {
    if((typeof $.colorbox == 'function') && ($("#popup").length == 0)) {
      $.colorbox({
        Title: "Results.",
        height: "80%",
        width: "80%",
        opacity: ".25",
        inline: true,
        href:"#" + Drupal.openlayers.openlayers_behavior_wmsgetlegend.getlegend_htmlelement}
      );
    } else {
      $("#" + Drupal.openlayers.openlayers_behavior_wmsgetlegend.getlegend_htmlelement).parent().css("display", "block");
    }
    if (Drupal.openlayers.openlayers_behavior_wmsgetlegend.getlegend_usevisiblelayers == false) { 
      document.getElementById(Drupal.openlayers.openlayers_behavior_wmsgetlegend.getlegend_htmlelement).innerHTML = Drupal.t('Searching...');
      return; 
    }
  
    if (layernames.length == 0 ) {
      document.getElementById(Drupal.openlayers.openlayers_behavior_wmsgetlegend.getlegend_htmlelement).innerHTML = Drupal.t('No layer selected');
      return;
    } else {
      document.getElementById(Drupal.openlayers.openlayers_behavior_wmsgetlegend.getlegend_htmlelement).innerHTML = Drupal.t('Getting Legend for ' + layernames);
    }
    return;
  };
 
  Drupal.openlayers.openlayers_behavior_wmsgetlegend.fillHTML = function(result) {
    $('#' + Drupal.openlayers.openlayers_behavior_wmsgetlegend.getlegend_htmlelement).html(result);
    Drupal.attachBehaviors($('#' + Drupal.openlayers.openlayers_behavior_wmsgetlegend.getlegend_htmlelement));
  };

  Drupal.openlayers.openlayers_behavior_wmsgetlegend.fillHTMLerror = function(result) {
    $('#' + Drupal.openlayers.openlayers_behavior_wmsgetlegend.getlegend_htmlelement).html(result);
    Drupal.attachBehaviors($('#' + Drupal.openlayers.openlayers_behavior_wmsgetlegend.getlegend_htmlelement));
  };
})(jQuery);
